<!-- start -->
@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
<!-- test nav -->
<nav class="navbar navbar-expand-lg navbar-top1 " style="position:fixed"> 

  <a class="navbar-brand" href="/">cloud Logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container containner-width">  
  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2 search-ds" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-envelope fa-clr"><span class="badge badge-pill badge-success">1</span> &nbsp;</i></i></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-bell fa-clr"><span class="badge badge-pill badge-success">1</span> &nbsp;</i></a>
      </li>
      <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-exclamation-circle fa-clr"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    @if(Auth::check())
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
      <i class="fa fa-user fa-clr"></i>&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>  
      @endif   
</div>
  </div>
</nav>

<!-- enf test nav -->

<div class="nav-vn">
<a class="active" href="/admin"><img src="/images/logo.png" height="30px"></a>
<ul style="    list-style: none;
    padding-top: 70px;
    padding-left: 29px;
    text-align: left;
    color: white;
    font-family: Asap !important;
    /* width: 106%; */
    letter-spacing: 1px;

">
  <li></li>
  <li><i >&nbsp;</i><a href="/admin" class="fa fa-tachometer fa-clr">Dashborad</a></li>
    <li><i ></i>&nbsp;<a href="cust-list" class="fa fa-list fa-clr">Customers</a></li>
    <li><i>&nbsp;</i><a href="/order" class="fa fa-cart-arrow-down fa-clr">Orders</a></li>
    <li><i ></i>&nbsp;<a href="/invoice" class="fa fa-list fa-clr">Invoice</a></li>
    <li><i >&nbsp;</i><a href="/admin-host" class="fa fa-cart-arrow-down  fa-clr">Hosting</a></li>
    <li><i >&nbsp;</i><a href="ad-Domain" class="fa fa-cart-arrow-down  fa-clr">Domian</a></li>
    <li><i >&nbsp;</i><a  href="ad-website" class="fa fa-cart-arrow-down fa-clr">Website</a></li>
</ul>
<div>
</div><div>
</div><div>
</div>
<!-- test nav -->

</div>
<!-- enf test nav -->

<div class="nav-con">
  
<!-- start content -->
<div class="ad-content">
  <div class="card">
    <h5 class="card-header table-clr">Website Overview</h5>
      <div class="card-body">
       <div class="row">
       <div class="col-sm-4" >
       <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h2 ><i class="fa fa-users fa-2x" aria-hidden="true"></i></h2>
    <p class="card-text"><h2>{{$users}} Users</h2></p>
    <a href="/cust-list" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>

    <div class="col-sm-4" >
    <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5><i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
</h5>
    <p class="card-text"><h2>{{$invoices}} Invoice</h2></p>
    <a href="/invoice" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
    <div class="col-sm-4" >
    <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5><i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
</h5>
    <p class="card-text"><h2>{{$orders}} orders</h2></p>
    <a href="/order" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
       </div>
      </div>
    </div>
<!-- </div> -->
<!-- table start -->
<!-- <div class="table-mar"> -->
  <br><br><br>
<!-- <div class="ad-content"> -->

<!-- saart modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Mail Sending</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
    <form action="#" mathod="post">
      <div class="modal-body table">
        <label>Mail-Address</label><input type="text" name="email" value="mathtn@gmail.com">
        <br><br>
        Message&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea> mathan</textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" type="button" class="button-sm" value="SendMail">
      </div>
    </form>
    </div>
  </div>
</div>
</div>
<!-- end modal -->
</div>
<!-- end content -->
<div class="footer"><center>Developed by Anlyz360 
</center>
  
</div>
@endsection

