@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
<head>
      <!---shared hosting--->
      <link re="stylesheet" href="css/shared.css">
    <!-----shared hosting end--->
    <!-- start top nav -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="container containner-width">
  <a class="navbar-brand" href="#">MySmart Cloud logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Currency
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown" >
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-globe"></i>
        Language
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item" style="display: block ruby;">
      <i class="fa fa-user fa-h"></i>
        <a class="nav-link fa-s" href="#">Login/Signup</a>
      </li>
      </li>
      <li class="nav-item " style="display: block ruby;">
        <i class="fa fa-shopping-cart fa-h" ></i>
         
        <a class="nav-link fa-s" href="check-out">Cart   @if(Session::has('cart'))<span class="badge badge-pill badge-success">@if(session('cart'))        
<?php                 
          $sum = 0;
          foreach(session('cart') as $id => $details)
          {
          $sum+= $details['quantity'];
          } 
          echo $sum;
?>
  @endif</span>@endif</a>
      </li>
      
    </ul>
   
  </div>
</div>
</nav>
<!-- end top nav -->
<!-- start base nav -->

<nav class="navbar navbar-expand-lg navbar-light bg-light navbar navbar-dark bg-dark">
<div class="container containner-width">  
<a class="navbar-brand" href="#">Dropdown </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link active" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Domain</a>
      <div class="dropdown-content">
        <a href="#">Registration</a>
        <a href="#">Add-ons</a>
        <a href="#">Transfer</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Website<span class="badge badge-pill badge-success">new</span></a>
      <div class="dropdown-content">
        <a href="#">Build your Website</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Hosting</a>
      <div class="dropdown-content">
        <a href="host">Shared Hosting</a>
        <a href="#">Servers</a>
        <a href="#">Reseller Hosting</a>
        <a href="#">Tools</a>
    </div>
    </div>
      </li>
      <li class="nav-item">
        <a class="nav-link dropdown" href="cloud">Cloud</a>
      </li>
      <li class="nav-link">
      <div class="email">
      <a class="emailbtn">Email</a>
      <div class="email-content">
        <a href="#">G Suite<br>(formerly Google Apps for Work)</a>
        <a href="#">Business Email</a>
        <a href="#">Enterprise Email</a>
    </div>
    </div>
      </li>
    </ul>   
  </div>
  <div>
</nav>
<!-- end base nav -->
<!-- session data start -->
@if(session()->has('addcart'))
    <div class="alert alert-success"> 
    {!! session('addcart') !!}
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-warning"> 
    {!! session('error') !!}
    </div>
@endif
@if(session()->has('create-acc'))
    <div class="alert alert-success"> 
    {!! session('create-acc') !!}
    </div>
@endif
@if(session()->has('pay-success'))
    <div class="alert alert-success"> 
    {!! session('pay-success') !!}
    </div>
@endif
@if(session()->has('pay-fail'))
    <div class="alert alert-warning"> 
    {!! session('pay-fail') !!}
    </div>
@endif
@if(session()->has('cart-empt'))
    <div class="alert alert-success"> 
    {!! session('cart-empt') !!}
    </div>
@endif
<!-- session data end -->
<div class="jumbotron1">
<div class="container containner-width">
  <h2 class="display-4">MySmart Cloud Hosting </h2>
  <p class="lead">Shared Hosting - At it's Simplest Best Get your Business Online in an Affordable way.</p>
  <hr class="my-4">
  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
  </p>
</div>
</div>
@foreach($products->chunk(3) as $productchunk)
@endforeach
<div class="shared_pricing">
  <div class="shared_card_main_heading">
    <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="shared_card_main_heading_h1">OUR BEST PEICING</h1>
      </div>
    </div>
  </div>
        </div>
<div class="container">
    <div class="row">
@foreach($productchunk as $product)
        <div class="col-sm-4">
            <div class="card card_shared" style="width: 20rem;">
                <div class="card-body" style="text-align: center;">
                <img src="/images/pricing_image.png" class="img_hosting">
                <input type="hidden" name="pid" value="{{$product->id}}">
                    <h5 class="card-title shared_cared_title">{{$product->product_name}}</h5>
                    <input type="hidden" name="product_name" value="{{$product->product_name}}">
                    <h6 class="card-subtitle mb-2 text-muted color_shared">{{$product->package_name}}</h6>
                    <input type="hidden" name="package_name" value="{{$product->package_name}}">
                     <p class="card-text" class="color_shared">{{$product->description}}</p>
                     <input type="hidden" name="description" value="{{$product->description}}">
                     <p class="card-text color_shared">Unmetered Disk Space</p>
                     <p class="card-text color_shared">Unmetered Data Transfer</p>
                     <p class="card-text color_shared">Unlimited Email Accounts </p>
                     <p class="card-text color_shared">Starting at</p>
                    <h4 style="font-weight: 900;letter-spacing: 1px;">{{$product->price}}/Mo<h4>
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <button class="btn btn-primary" data-id='{{ $product->id }}' data-product='{{ $product->product_name }}' data-package='{{ $product->package_name }}' data-description='{{ $product->description }}' data-price='{{ $product->price }}' data-toggle="modal" data-target="#addcart">Add Cart</button>

                </div>
            </div>  
        </div>
        <div class="col-sm-4">
            <div class="card card_shared" style="width: 20rem;">
                <div class="card-body" style="text-align: center;">
                <img src="/images/pricing_image.png" class="img_hosting">
                <input type="hidden" name="pid" value="{{$product->id}}">
                    <h5 class="card-title shared_cared_title">{{$product->product_name}}</h5>
                    <input type="hidden" name="product_name" value="{{$product->product_name}}">
                    <h6 class="card-subtitle mb-2 text-muted">{{$product->package_name}}</h6>
                    <input type="hidden" name="package_name" value="{{$product->package_name}}">
                     <p class="card-text">{{$product->description}}</p>
                     <input type="hidden" name="description" value="{{$product->description}}">
                     <p class="card-text color_shared">Unmetered Disk Space</p>
                     <p class="card-text color_shared">Unmetered Data Transfer</p>
                     <p class="card-text color_shared">Unlimited Email Accounts </p>
                     <p class="card-text color_shared">Starting at</p>
                    <h4 style="font-weight: 900;letter-spacing: 1px;">{{$product->price}}/Mo<h4>
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <button class="btn btn-primary" data-id='{{ $product->id }}' data-product='{{ $product->product_name }}' data-package='{{ $product->package_name }}' data-description='{{ $product->description }}' data-price='{{ $product->price }}' data-toggle="modal" data-target="#addcart">Add Cart</button>

                </div>
            </div>  
        </div>
        <div class="col-sm-4">
            <div class="card card_shared" style="width: 20rem;">
                <div class="card-body" style="text-align: center;">
                <img src="/images/pricing_image.png" class="img_hosting">
                <input type="hidden" name="pid" value="{{$product->id}}">
                    <h5 class="card-title shared_cared_title">{{$product->product_name}}</h5>
                    <input type="hidden" name="product_name" value="{{$product->product_name}}">
                    <h6 class="card-subtitle mb-2 text-muted">{{$product->package_name}}</h6>
                    <input type="hidden" name="package_name" value="{{$product->package_name}}">
                     <p class="card-text">{{$product->description}}</p>
                     <input type="hidden" name="description" value="{{$product->description}}">
                     <p class="card-text color_shared">Unmetered Disk Space</p>
                     <p class="card-text color_shared">Unmetered Data Transfer</p>
                     <p class="card-text color_shared">Unlimited Email Accounts </p>
                     <p class="card-text color_shared">Starting at</p>
                    <h4 style="font-weight: 900;letter-spacing: 1px;">{{$product->price}}/Mo<h4>
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <button class="btn btn-primary" data-id='{{ $product->id }}' data-product='{{ $product->product_name }}' data-package='{{ $product->package_name }}' data-description='{{ $product->description }}' data-price='{{ $product->price }}' data-toggle="modal" data-target="#addcart">Add Cart</button>

                </div>
            </div>  
        </div>
        </form>
        @endforeach    
    </div>    
</div>
</div>
<!-- 2nd modal edit-->
<div class="modal fade" id="addcart" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Specify a domain name for your order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/add-cart" action="post">
      {{csrf_field()}}
        <div class="modal-body">
        <div class="form-group">
                <label for="product_id"></label>
                <input type="hidden" class="form-control" name="product_id" id="product_id" readonly>
             </div>
             <div class="form-group">
                <label for="product_name"></label>
                <input type="hidden" class="form-control" name="product_name" id="product_name" readonly>
             </div>
             <div class="form-group">
                <label for="domain_name">Enter Your Domain</label>
                <input type="text" class="form-control" name="domain_name" id="domain_name" required >
             </div>
             <div class="form-group">
                <label for="package_name"></label>
                <input type="hidden" class="form-control" name="package_name" id="package_name" required>
             </div>
             <div class="description">
                <label for="description"></label>
                <input type="hidden" class="form-control" name="description" id="description" required>
             </div>
             <div class="form-group">
                <label for="price"></label>
                <input type="hidden" class="form-control" name="price" id="price" required>
             </div>
         </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Continue to checkout</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- endModal -->
<span class="badge"></span>

<div class="host-c">

    <div class="container">
      <center>
        <h1 style="font-size:57px;font-weight:900;padding: 51px 0px;"> OUR BEST SERVICE</h1>
      </center> 
             <div class="row"> 
            <div class="col"><center>
             <img src="/images/choose-us-1.png">
            <br><br>
            <h3>
              Lightning Fast Website
            </h3>
            <p>Our web application accelerator, 
              powered by Varnish Cache,
              ensures the maximum performance of your website at all times!</p>
              </center>
              </div>
            <div class="col">
            <center>
            <img src="/images/choose-us-2.png">
            <br><br>
            <h3>
            Email included
            </h3>
            <p>
            Advanced email management features in cPanel allow you manage your emails, 
            mailing lists and more without any hassles.</p>
            </div>
            <div class="col"><center>
            <img src="/images/choose-us-3.png">
            <h3>cPanel for Management</h3>
            <p>
            Our web application accelerator, 
              powered by Varnish Cache, 
              ensures the maximum performance of your website at all times!</p></center></div>
          </div>
    </div>
   <!--- <br>
    <center>
        <h3>Install a Shopping Cart, Photo Gallery, a Blog or any other module in just 1 click</h3>
    50+ Plugins - Powered by Softaculous</center>
    <br><br>-->
    </div>
    <div class="foot">
  <div class="container">
     <div class="row">
        <div class="col-sm-3">
         <div class="service-app">Application Services</div>
         <h4 class="contact"><div class="contact1">Managed Applicatin Services</div>
          <div class="contact1">Business Analytics</div>
          <div class="contact1">Application Development</h4>
         </div>
         <div class="col-sm-3">
            <div class="service-app">Managed Cloud<br></div>
            <h3 class="contact">
              <div class="contact1">Microsoft Azure</div>
              <div class="contact1">Amazon Web Services</div>
              <div class="contact1">Google Cloud Platform</div>
              <div class="contact1">Kubemetes</div></h3>
         </div>
         <div class="col-sm-3">
            <div class="service-app">Managed Services</div>
            <h3 class="contact">
              <div class="contact1">Hosting Services</div>
              <div class="contact1">Managed Infrastructure</div>
              <div class="contact1">DevOps Solutions</div>
              <div class="contact1">Private Services</div>
              <div class="contact1">Architecture</div>
              <div class="contact1">Migration</div>
               </h3>
         </div>
        <div class="col-sm-3">
          <div class="service-app">ABOUT & Support</div>
            <h3 class="contact">
              <div class="contact1">Why Anlyz360</div>
              <div class="contact1">Careers</div>
              <div class="contact1">Content Information</div>
               </h3>
         </div> 
     </div><br>
     <div class="row">
       <div class="col-sm-4">
         <div class="row">
       <div class="col-sm-3">Home</div>
       <div class="col-sm-3">Services</div>
       <div class="col-sm-3">Products</div>
       <div class="col-sm-3">About</div>
     </div>
     </div>
     <div class="col-sm-8">

     </div>
    </div>
</div>
</div>
<div class="footer">
  <h3>Developed by anlyz360</h3>
</div>
@if(session('cart'))        
<?php                 
          $sum = 0;
          foreach(session('cart') as $id => $details)
          {
          $sum+= $details['quantity'];
          } 
          echo $sum;
?>
  @endif
@endsection
