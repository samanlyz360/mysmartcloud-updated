@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
    <!-- start top nav -->
    <nav class="navbar navbar-expand-lg navbar-light ">
<div class="container containner-width">
<i class="fa fa-envelope"></i>&nbsp;support@mysmartcloud.com
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Currency
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown" >
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-globe"></i>
        Language
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item" style="display: block ruby;">
        @if(Auth::check())
          @else
            <i class="fa fa-user fa-h"></i>
            <a class="nav-link fa-s" href="login">Login/Signup</a>
         @endif
      </li>
      </li>
      <li class="nav-item" style="display: block ruby;">
        <i class="fa fa-shopping-cart fa-h" ></i>
        
        <a class="nav-link fa-s" href="check-out">Cart   @if(Session::has('cart'))<span class="badge badge-pill badge-success">new</span>@endif</a>
      </li>
      
    </ul>
   
  </div>
</div>
</nav>
<!-- end top nav -->
<!-- start base nav -->

<nav class="navbar navbar-expand-lg navbar-light bg-light navbar navbar-dark bg-dark">
<div class="container containner-width">  
<a class="navbar-brand" href="#">Dropdown </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link active" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Domain</a>
      <div class="dropdown-content">
        <a href="#">Registration</a>
        <a href="#">Add-ons</a>
        <a href="#">Transfer</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Website<span class="badge badge-pill badge-success">new</span></a>
      <div class="dropdown-content">
        <a href="#">Build<span style="color:transparent">_</span>your<span style="color:transparent">_</span>Website</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Hosting</a>
      <div class="dropdown-content">
        <a href="host">Shared Hosting</a>
        <a href="#">Servers</a>
        <a href="#">Reseller Hosting</a>
        <a href="#">Tools</a>
    </div>
    </div>
      </li>
      <li class="nav-item">
        <a class="nav-link dropdown" href="cloud">Cloud</a>
      </li>
      <li class="nav-link">
      <div class="email">
      <a class="emailbtn">Email</a>
      <div class="email-content">
        <a href="#">G Suite<br>(formerly Google Apps for Work)</a>
        <a href="#">Business Email</a>
        <a href="#">Enterprise Email</a>
    </div>
    </div>
      </li>
    </ul>  
    @if(Auth::check())
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="margin:-3px 57px 0px 0px;">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>  
      @endif   
  </div>
  <div>
</nav>
@if(session()->has('cart-empt'))
    <div class="alert alert-success"> 
    {!! session('cart-empt') !!}
    </div>
@endif
<!-- end base nav -->

<div class="banner-top">
<div class="container">
    <div class="row">
        <div class="col-sm-12">
        <div class="wrap">
        <h1 class="banner_h1">Get Your Domain Search</h1>
   <div class="search">
      <input type="text" class="searchTerm" placeholder="What are you looking for?">
      <button type="submit" class="searchButton">
        <i class="fa fa-search"></i>
     </button>
   </div>
</div>
        </div>
    </div>

</div>

</div>
<div class="name_prices">
    <div class="container">
<div class="row">

    <div class="col-sm-12" style="display: flex;">
            <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.com</p>
              </div> 
              <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.in</p>
              </div> 
              <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.org</p>
              </div> 
              <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.net</p>
              </div> 
              <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.online</p>
              </div> 
              <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.club</p>
              </div> 
              <div class="name_and_name">
                <span class="name_price_span">Rs. 996.83</span>
           <p class="doamin_names">.co</p>
              </div> 
              
              </div>
               
</div> 
    </div>
</div>
</div>
<div class="idn_search">
    <div class="container">
     <div class="idn_search_header">
        <div class="row">
            <div class="col-sm-12">
              <h1 class="idn_search_header_h1">IDN SEARCH</h1>
            </div>
        </div>
     </div>
     <div class="domain_extensions">
         <div class="container">
             <div class="row">
                <h1 class="supported_extensions"> Domain Extensions</h1> 
             </div>
             <div class="row">
                 <div class="col-sm-4">
                 <label class="container2">.in
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.com
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.org
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.co
  <input type="checkbox">
  <span class="checkmark"></span>
</label>

<label class="container2">.xyz
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.de
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.host
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.press
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.press
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
      
<label class="container2">.eu
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.cc
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.org
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.co
  <input type="checkbox">
  <span class="checkmark"></span>
</label>

      
   
                 </div>
                 <div class="col-sm-4">
                 <label class="container2">.co.in
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.in.net
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.xyz
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.de
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.host
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.press
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
      
<label class="container2">.eu
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.cc
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.org
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.co
  <input type="checkbox">
  <span class="checkmark"></span>
</label>

<label class="container2">.xyz
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.de
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.host
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.press
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
   
                 </div>
                 <div class="col-sm-4">
                 <label class="container2">.xyz
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.de
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.host
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.org
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.co
  <input type="checkbox">
  <span class="checkmark"></span>
</label>

<label class="container2">.xyz
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container2">.de
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.host
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.press
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
<label class="container2">.press
  <input type="checkbox">
  <span class="checkmark"></span>
</label>
      
                 </div>
             </div>
         </div>
     </div>
    </div>
</div>
<div class="new_domains">
    <div class="container">
    <div class="row">
        <div class="col-sm-10">
<h1 class="new_domains_h1">1000s of new domains are coming soon</h1>
        </div>
        <div class="col-sm-2">
        <button type="button" class="btn btn-success new_domains_btn">Get In</button>
        </div>
    </div>
</div>
</div>
<div class="free_ads">
<div class="container">
<div class="idn_search_header">
        <div class="row">
            <div class="col-sm-12">
              <h1 class="idn_search_header_h1">FREE Add-ons with every Domain Name!</h1>
            </div>
        </div>
     </div>
<div class="row">
<div class="col-sm-3">
    <div class="card card_1" style="background:orange;">
        <div class="cars_pic" >
            <img src="/images/10.png" height="104px">
        </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> 
<div class="col-sm-3">
    <div class="card card_1" style="background:purple;">
        <div class="cars_pic" >
          <img src="/images/11.png" height="104px">
</div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> <div class="col-sm-3">
    <div class="card card_1" style="background:#ffc107;">
        <div class="cars_pic">
        <img src="/images/10.png" height="104px">
          </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> <div class="col-sm-3">
    <div class="card card_1" style="background:blueviolet;">
        <div class="cars_pic">
        <img src="/images/11.png" height="104px">
           </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> 
</div>
<br>
<div class="row">
<div class="col-sm-3">
    <div class="card card_1" style="background:green;">
        <div class="cars_pic">
        <img src="/images/11.png" height="104px">
        </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> 
<div class="col-sm-3">
    <div class="card card_1" style="background: deepskyblue;">
        <div class="cars_pic">
        <img src="/images/10.png" height="104px">
        </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> <div class="col-sm-3">
    <div class="card card_1" style="background: #eb1515;">
        <div class="cars_pic">
        <img src="/images/11.png" height="104px">
        </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> <div class="col-sm-3">
    <div class="card card_1" style="background:#eb155b;">
        <div class="cars_pic">
        <img src="/images/10.png" height="104px">
        </div>
           <div class="card-body">
            <div class="card_conent">
            <h5 class="free_ads_h5">Free Email Account</h5>
            </div>
         </div>
        </div>
</div> 
</div>
<br>
</div>
</div>
</div>
</div>
</div>

</div>

<div class="foot">
  <div class="container">
     <div class="row">
        <div class="col-sm-3">
         <div class="service-app">Application Services</div>
         <h4 class="contact"><div class="contact1">Managed Applicatin Services</div>
          <div class="contact1">Business Analytics</div>
          <div class="contact1">Application Development</h4>
         </div>
         <div class="col-sm-3">
            <div class="service-app">Managed Cloud<br></div>
            <h3 class="contact">
              <div class="contact1">Microsoft Azure</div>
              <div class="contact1">Amazon Web Services</div>
              <div class="contact1">Google Cloud Platform</div>
              <div class="contact1">Kubemetes</div></h3>
         </div>
         <div class="col-sm-3">
            <div class="service-app">Managed Services</div>
            <h3 class="contact">
              <div class="contact1">Hosting Services</div>
              <div class="contact1">Managed Infrastructure</div>
              <div class="contact1">DevOps Solutions</div>
              <div class="contact1">Private Services</div>
              <div class="contact1">Architecture</div>
              <div class="contact1">Migration</div>
               </h3>
         </div>
        <div class="col-sm-3">
          <div class="service-app">ABOUT & Support</div>
            <h3 class="contact">
              <div class="contact1">Why Anlyz360</div>
              <div class="contact1">Careers</div>
              <div class="contact1">Content Information</div>
               </h3>
         </div> 
     </div><br>
     <div class="row">
       <div class="col-sm-4">
         <div class="row">
       <div class="col-sm-3">Home</div>
       <div class="col-sm-3">Services</div>
       <div class="col-sm-3">Products</div>
       <div class="col-sm-3">About</div>
     </div>
     </div>
     <div class="col-sm-8">

     </div>
    </div>
</div>
</div>
<div class="footer">
  <h3>Developed by anlyz360</h3>
</div>








@endsection