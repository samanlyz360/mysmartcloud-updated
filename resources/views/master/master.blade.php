<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{URL::to('css/drop.css')}}">
    <link rel="stylesheet" href="css/domain.css">
    <!---shared hosting--->
    <link re="stylesheet" href="css/shared.css">
    <!-----shared hosting end--->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">   <!----fonts--->
   <link href="https://fonts.googleapis.com/css?family=Spartan&display=swap" rel="stylesheet">
   <!---mysmart_h1--->
   <link href="https://fonts.googleapis.com/css?family=Asap|Parisienne|Poppins|Russo+One|Spartan&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Asap|Parisienne|Russo+One|Spartan&display=swap" rel="stylesheet">
   <!---mysmartcloud-->
   <link href="https://fonts.googleapis.com/css?family=Asap|Spartan&display=swap" rel="stylesheet">
   <!----fonts--->
    <title>@yield('title')</title>
</head>
<body>
    
   
    @yield('content')  
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    @yield('scripts')
    <!-- product edit -->
    <script>
    $('#edit').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            var product = button.data('product')
            var package = button.data('package')
            var description = button.data('description')
            var price = button.data('price')
            
             // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #product_id').val(id)
            modal.find('.modal-body #product_name').val(product)
            modal.find('.modal-body #package_name').val(package)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #price').val(price)
})
    </script>
    <!-- admin cust detail -->
     <script>
    $('#custdetail').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var name = button.data('name')
            var cname = button.data('cname')
            var email = button.data('email')
            var gst = button.data('gst')
            var add = button.data('add')
            var add1 = button.data('add1')
            var city = button.data('city')
            var zip = button.data('zip')
            var state = button.data('state')
            var country = button.data('country')
            var mbl = button.data('mbl')
            
             // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #cname').val(cname)
            modal.find('.modal-body #email').val(email)
            modal.find('.modal-body #gst').val(gst)
            modal.find('.modal-body #add').val(add)
            modal.find('.modal-body #add1').val(add1)
            modal.find('.modal-body #city').val(city)
            modal.find('.modal-body #zip').val(zip)
            modal.find('.modal-body #state').val(state)
            modal.find('.modal-body #country').val(country)
            modal.find('.modal-body #mbl').val(mbl)



})
    </script>
    <!-- product add -->
        <script>
    $('#addcart').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            var product = button.data('product')
            var package = button.data('package')
            var description = button.data('description')
            var price = button.data('price')
            
             // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #product_id').val(id)
            modal.find('.modal-body #product_name').val(product)
            modal.find('.modal-body #package_name').val(package)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #price').val(price)
})
    </script>
    <!-- admin order detail -->
            <script>
    $('#adorder').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            // console.log(id);
            var product_id = button.data('product_id')
            var total_price = button.data('total_price')
            var payment_status = button.data('payment_status')
             var domain = button.data('domain')
             var txnid = button.data('txnid')
             var cpanel_user = button.data('cpanel_user')
             var cpanel_pwd = button.data('cpanel_pwd')
             var product_name= button.data('product_name')
             var package_name = button.data('package_name')
             var name = button.data('name')
             console.log(name);
             var cname = button.data('cname')
             var email = button.data('email')
            
            
             // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #id').val(id)
            modal.find('.modal-body #product_id').val(product_id)
            modal.find('.modal-body #total_price').val(total_price)
            modal.find('.modal-body #payment_status').val(payment_status)
            modal.find('.modal-body #domain').val(domain)
            modal.find('.modal-body #txnid').val(txnid)
            modal.find('.modal-body #cpanel_user').val(cpanel_user)
            modal.find('.modal-body #cpanel_pwd').val(cpanel_pwd)
            modal.find('.modal-body #product_name').val(product_name)
            modal.find('.modal-body #package_name').val(package_name)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #cname').val(cname)
            modal.find('.modal-body #email').val(email)

})
    </script>
    <!-- admin invoice details -->
    <script>
    $('#adinvoice').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            var name = button.data('name')
            var cname = button.data('cname')
            var email = button.data('email')
            var gst = button.data('gst')
            var add = button.data('add')
            var add1 = button.data('add1')
            var city = button.data('city')
            var zip = button.data('zip')
            var state = button.data('state')
            var country = button.data('country')
            var mbl = button.data('mbl')
            var date = button.data('date')
            var product = button.data('product')
            var product_name = button.data('product_name')
            var package = button.data('package')
            var description = button.data('description')
            var price = button.data('price')
            var total_price = button.data('in_total_price')
            var gst = button.data('in_gst')

             // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #cname').text(cname)
            modal.find('.modal-body #email').val(email)
            modal.find('.modal-body #gst').val(gst)
            modal.find('.modal-body #add').text(add)
            modal.find('.modal-body #add1').text(add1)
            modal.find('.modal-body #city').text(city)
            modal.find('.modal-body #zip').text(zip)
            modal.find('.modal-body #state').text(state)
            modal.find('.modal-body #country').text(country)
            modal.find('.modal-body #mbl').val(mbl)
            modal.find('.modal-body #id').text(id)
            modal.find('.modal-body #date').text(date)
            modal.find('.modal-body #product').text(product)
            modal.find('.modal-body #product_name').text(product_name)
            modal.find('.modal-body #package').text(package)
            modal.find('.modal-body #description').text(description)
            modal.find('.modal-body #price').text(price)
            modal.find('.modal-body #total_price').text(total_price)
            modal.find('.modal-body #tot').text(total_price)
            modal.find('.modal-body #gst').text(gst)





})
    </script>
</body>
</html>