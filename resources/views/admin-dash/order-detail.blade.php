<!-- start -->
@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
<!-- test nav -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script>
    $('#adorder').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id')
            // console.log(id);
            var product_id = button.data('product_id')
            var total_price = button.data('total_price')
            var payment_status = button.data('payment_status')
             var domain = button.data('domain')
             var txnid = button.data('txnid')
             var cpanel_user = button.data('cpanel_user')
             var cpanel_pwd = button.data('cpanel_pwd')
             var product_name= button.data('product_name')
             var package_name = button.data('package_name')
             var name = button.data('name')
             console.log(name);
             var cname = button.data('cname')
             var email = button.data('email')
            
            
             // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-body #id').val(id)
            modal.find('.modal-body #product_id').val(product_id)
            modal.find('.modal-body #total_price').val(total_price)
            modal.find('.modal-body #payment_status').val(payment_status)
            modal.find('.modal-body #domain').val(domain)
            modal.find('.modal-body #txnid').val(txnid)
            modal.find('.modal-body #cpanel_user').val(cpanel_user)
            modal.find('.modal-body #cpanel_pwd').val(cpanel_pwd)
            modal.find('.modal-body #product_name').val(product_name)
            modal.find('.modal-body #package_name').val(package_name)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #cname').val(cname)
            modal.find('.modal-body #email').val(email)

})
    </script>
 <style>
body
{
    font-family :Poppins ;
}
.modal-body
{
    letter-spacing: 1px;
    font-size: 18px;
}
.modal-content {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 955px;
    margin-left: -227px;
    pointer-events: auto;
    /* margin-left: auto; */
    /* margin-right: 33px; */
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: .3rem;
    outline: 0;
}
</style>
<nav class="navbar navbar-expand-lg navbar-top1 " style="position:fixed"> 
<div class="container containner-width ">
  <a class="navbar-brand" href="#">Dashborad</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container containner-width">  
  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2 search-ds" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-envelope fa-clr"><span class="badge badge-pill badge-success">1</span> &nbsp;</i></i></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-bell fa-clr"><span class="badge badge-pill badge-success">1</span> &nbsp;</i></a>
      </li>
      <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-exclamation-circle fa-clr"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    @if(Auth::check())
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
      <i class="fa fa-user fa-clr"></i>&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="margin:-3px 57px 0px 0px;">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>  
      @endif   
</div>
  </div>
  </div>
</nav>

<!-- enf test nav -->

<div class="nav-vn">
<a class="active" href="/admin"><img src="/images/logo.png" height="30px"></a>
<ul style="    list-style: none;
    padding-top: 70px;
    padding-left: 29px;
    text-align: left;
    color: white;
    /* width: 106%; */
    letter-spacing: 1px;

">
  <li></li>
  <li><i >&nbsp;</i><a href="/admin" class="fa fa-tachometer fa-clr">Dashborad</a></li>
    <li><i ></i>&nbsp;<a href="cust-list" class="fa fa-list fa-clr">Customers</a></li>
    <li><i>&nbsp;</i><a href="order" class="fa fa-cart-arrow-down fa-clr">Orders</a></li>
    <li><i ></i>&nbsp;<a href="/invoice" class="fa fa-list fa-clr">Invoice</a></li>
    <li><i >&nbsp;</i><a href="/admin-host" class="fa fa-cart-arrow-down  fa-clr">Hosting</a></li>
    <li><i >&nbsp;</i><a href="ad-Domain" class="fa fa-cart-arrow-down  fa-clr">Domian</a></li>
    <li><i >&nbsp;</i><a  href="ad-website" class="fa fa-cart-arrow-down fa-clr">Website</a></li>
</ul>
<div>
</div><div>
</div><div>
</div>
<!-- test nav -->

</div>
<!-- enf test nav -->

<div class="nav-con">
  
<!-- start content -->
<div class="ad-content">
<!-- </div> -->
<!-- table start -->
<!-- <div class="table-mar"> -->
  <br><br><br>
<!-- <div class="ad-content"> -->
<div class="card">
<h5 class="card-header table-clr">Orders List</h5>
<br>
<table class="table ">
  <thead>
    <tr>
      <th scope="col">Order ID</th>
      <th scope="col">Product_id</th>
      <th scope="col">Total Ammount</th>
      <th scope="col">payment status</th>
      <th scope="col">Show Full Detail</th>
    </tr>
  </thead>
  <tbody>
@foreach($orders as $order)
    <tr>
      <td scope="col">{{ $order->id }}</td>
      <td scope="col">{{ $order->product_id }}</td>
      <td scope="col">{{ $order->total_price }}</td>
      <td scope="col">{{ $order->payment_status }}
      <!-- {{ $order->domain }}
      {{ $order->txnid }}
      {{ $order->cpanel_user }}
      {{ $order->cpanel_pwd }}
      {{ $order->product->product_name }}
      {{ $order->product->price }}
      {{ $order->product->package_name }}
      {{ $order->user->name}}
      {{ $order->user->cname}}
      {{ $order->user->email}} -->



      
      
      
      </td>
      <td scope="col"><button class="btn btn-primary" data-id="{{ $order->id }}" data-product_id="{{ $order->product_id }}" data-total_price="{{ $order->total_price }}"data-payment_status="{{ $order->payment_status }}"
      data-domain="{{ $order->domain }}"data-txnid="{{ $order->txnid }}"data-cpanel_user="{{ $order->cpanel_user }}"data-cpanel_pwd="{{ $order->cpanel_pwd }}"
      data-product_name="{{ $order->product->product_name }}"data-package_name="{{ $order->product->package_name }}"data-name="{{ $order->user->name}}"data-cname="{{ $order->user->cname}}"
      data-email="{{ $order->user->email }}"  data-toggle="modal" data-target="#adorder">Detail</button></td>

    </tr>
    @endforeach
  </tbody>
</table>
</div>
<!-- end table -->
<!-- detail modal -->
 <!-- Modal -->
 <div class="modal fade" id="adorder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container">
          <div class="row">
              <div class="col-sm-12">
            <form>
                <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="id">Order Id:</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="id" aria-describedby="emailHelp" name="id">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="product_id">Product ID:</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="product_id" name="product_id" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="product_name">Product Name:</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="product_name" name="product_name" name="price" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="package_name">Package Name</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="package_name" name="package_name" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="total_price">Total Price</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="total_price" name="total_price" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="cpanel_user">Cpanel Username</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="cpanel_user" name="cpanel_user" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="cpanel_pwd">Cpanel Password</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="cpanel_pwd" name="cpanel_pwd" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                        <div class="row">
                        <!-- <div class="col-sm-6">    
                      <label for="id">User Id:</label>
                      </div>
                      <div class="col-sm-6">
                      <input type="text" class="form-control" id="i" name="name"aria-describedby="emailHelp">
                      </div> -->
                    </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">    
                      <label for="name">User Name:</label>
                      </div>
                      <div class="col-sm-6">
                      <input type="text" class="form-control" id="name" aria-describedby="emailHelp">
                      </div>
                    </div>
                    </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="email">Email</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="cname">Company Name</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="cname" name="cname" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="txnid">Transaction ID</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="txnid" name="txnid" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-sm-6">    
                  <label for="payment_status">Payment status</label>
                  </div>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" id="payment_status" name="payment_status" aria-describedby="emailHelp">
                  </div>
                </div>
                </div>
                </div>
                </div>


              </form>
          </div>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
<!-- end detail modal -->

</div>
<!-- end content -->

@endsection


