@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content') 
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Add New Product
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">New Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/samplepost" action="post">
      {{csrf_field()}}
        <div class="modal-body">
             <div class="form-group">
                <label for="product_name">product_name</label>
                <input type="text" class="form-control" name="product_name" id="product_name">
             </div>
             <div class="form-group">
                <label for="package_name">package_name</label>
                <input type="text" class="form-control" name="package_name" id="package_name">
             </div>
             <div class="description">
                <label for="description">description</label>
                <input type="text" class="form-control" name="description" id="description">
             </div>
             <div class="form-group">
                <label for="price">price</label>
                <input type="text" class="form-control" name="price" id="price">
             </div>
         </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- table -->
<table class="table">
@foreach($products as $product)
    <tr>
      <th scope="row">{{ $product->id }}</th>
      <td>{{ $product->product_name }}</td>
      <td>{{ $product->package_name }}</td>
      <td>{{ $product->description }}</td>
      <td>{{ $product->price }}</td>
      <td><button class="btn btn-primary" data-id='{{ $product->id }}' data-product='{{ $product->product_name }}' data-package='{{ $product->package_name }}' data-toggle="modal" data-target="#edit">Edit</button></td>
      <!-- <td><button type="button" class="button-sm" data-toggle="modal" data-target="#exampleModalCenter"> -->
            <!-- Edit -->
          <!-- </button> --></td>

    </tr>
    @endforeach

</table>
<!-- table -->
<!-- 2nd modal -->
<!-- Modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/sampleupdate" action="post">
      {{ method_field('PATCH') }}
      {{csrf_field()}}
        <div class="modal-body">
        <div class="form-group">
                <label for="product_id"></label>
                <input type="hidden" class="form-control" name="product_id" id="product_id" readonly>
             </div>
             <div class="form-group">
                <label for="product_name">product_name</label>
                <input type="text" class="form-control" name="product_name" id="product_name" readonly>
             </div>
             <div class="form-group">
                <label for="package_name">package_name</label>
                <input type="text" class="form-control" name="package_name" id="package_name">
             </div>
             <div class="description">
                <label for="description">description</label>
                <input type="text" class="form-control" name="description" id="description">
             </div>
             <div class="form-group">
                <label for="price">price</label>
                <input type="text" class="form-control" name="price" id="price">
             </div>
         </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
