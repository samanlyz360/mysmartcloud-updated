<!-- start -->
@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')

<!-- test nav -->
<nav class="navbar navbar-expand-lg navbar-light nav-back" > 

  <a class="navbar-brand" href="#">MySmart</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container containner-width">  
  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2 search-ds" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-envelope fa-clr"><span class="badge badge-pill badge-success">1</span> &nbsp;</i></i></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-bell fa-clr"><span class="badge badge-pill badge-success">1</span> &nbsp;</i></a>
      </li>
      <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-exclamation-circle fa-clr"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    @if(Auth::check())
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
      <i class="fa fa-user fa-clr"></i>&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="margin:-3px 57px 0px 0px;">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>  
      @endif   
</div>
  </div>
</nav>
</div>
<!-- enf test nav -->
<!-- ver-nav -->
<nav class="navbar1">
  <!-- <a href="/" id="navbar_icon1"> logo</a> -->
  <ul class="list1">
    <li><i class="fa fa-tachometer fa-2x fa-clr">&nbsp;</i><a href="/admin">Dashborad</a></li>
    <li><i class="fa fa-list fa-clr"></i>&nbsp;<a href="cust-list">Customers</a></li>
    <li><i class="fa fa-cart-arrow-down fa-2x fa-clr">&nbsp;</i><a href="order">Oreders</a></li>
    <li><i class="fa fa-list fa-clr"></i>&nbsp;<a href="/invoice">Invoice</a></li>
    <li><i class="fa fa-cart-arrow-down fa-2x fa-clr">&nbsp;</i><a href="/admin-host">Hosting</a></li>
    <li><i class="fa fa-cart-arrow-down fa-2x fa-clr">&nbsp;</i><a href="ad-Domain">Domian</a></li>
    <li><i class="fa fa-cart-arrow-down fa-2x fa-clr">&nbsp;</i><a href="ad-website">Website</a></li>
  </ul>
</nav>
<!-- enf -vernav -->
<!-- start content -->
<div class="ad-content">
  <div class="card">
    <h5 class="card-header table-clr">Website Overview</h5>
      <div class="card-body">
       <div class="row">
       <div class="col-sm-3" >
       <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h2 ><i class="fa fa-users fa-2x" aria-hidden="true"></i></h2>
    <p class="card-text"><h2>140 Users</h2></p>
    <a href="/cust-list" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
       <div class="col-sm-3" >
       <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h2 ><i class="fa fa-users fa-2x" aria-hidden="true"></i></h2>
    <p class="card-text"><h2>140 Users</h2></p>
    <a href="/cust-list" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
    <div class="col-sm-3" >
    <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5><i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
</h5>
    <p class="card-text"><h1>15000 orders</h1></p>
    <a href="/order" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
    <div class="col-sm-3" >
    <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5><i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
</h5>
    <p class="card-text"><h1>15000 orders</h1></p>
    <a href="/order" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
       </div>
      </div>
    </div>
</div>
<!-- table start -->
<!-- <div class="table-mar"> -->
  
<div class="ad-content">
<div class="card">
<h5 class="card-header table-clr">VIP Users</h5>
<br>
<table class="table ">
  <thead>
    <tr>
      <th scope="col">User ID</th>
      <th scope="col">Customer Name</th>
      <th scope="col">No_Of_Orders</th>
      <th scope="col">Total Amount</th>
      <th scope="col">No_of_Product</th>
      <th scope="col">Send Mail</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Sam</td>
      <td>10</td>
      <td>10,000</td>
      <td>2</td>
      <td><button type="button" class="button-sm" data-toggle="modal" data-target="#exampleModalCenter">
            Edit
          </button>
      </td>
    </tr>
    <tr>
    <th scope="row">2</th>
      <td>nj</td>
      <td>2</td>
      <td>20,000</td>
      <td>15</td>
      <td><button type="button" class="button-sm" data-toggle="modal" data-target="#exampleModalCenter">
            click
          </button>
      </td>
    </tr>
    <tr>
    <th scope="row">3</th>
      <td>sara</td>
      <td>20</td>
      <td>5000</td>
      <td>25</td>
      <td><button type="button" class="button-sm" data-toggle="modal" data-target="#exampleModalCenter">
           click
         </button>
      </td>
    </tr>
  </tbody>
</table>
</div>
<!-- end table -->
<!-- saart modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Mail Sending</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
    <form action="#" mathod="post">
      <div class="modal-body table">
        <label>Mail-Address</label><input type="text" name="email" value="mathtn@gmail.com">
        <br><br>
        Message&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea> mathan</textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" type="button" class="button-sm" value="SendMail">
      </div>
    </form>
    </div>
  </div>
</div>
</div>
<!-- end modal -->
<!-- end content -->
@endsection


