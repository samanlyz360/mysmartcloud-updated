<!-- start -->
@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
<!-- test nav -->
<nav class="navbar navbar-expand-lg navbar-top1 " style="position:fixed"> 

  <a class="navbar-brand" href="/">cloud Logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container containner-width">  
  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2 search-ds" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item ">
        <a class="nav-link" href="#"><i class="fa fa-envelope fa-clr" style="color: #001f8e;">&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-success">1</span> &nbsp;</i></i></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#">
        <i class="fa fa-bell fa-clr" style="color: #001f8e;">&nbsp;&nbsp;&nbsp;
        <span class="badge badge-pill badge-success">1</span> &nbsp;</i></a>
      </li>
      <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-exclamation-circle fa-clr" style="color: #001f8e;"></i>&nbsp;&nbsp;&nbsp;
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    @if(Auth::check())
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
      <i class="fa fa-user fa-clr"></i>&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>  
      @endif   
</div>
  </div>
</nav>

<!-- enf test nav -->

<div class="nav-vn">
<a class="active" href="/home"><img src="/images/logo.png" height="30px"></a>

<ul style="    list-style: none;
    padding-top: 70px;
    padding-left: 12px;
    padding-right: 10px;
    text-align: left;
    color: black;
    font-family:Poppins;
      letter-spacing: 1px;
">
  <p class="interface">Interface</p>
  <li style="display:flex;"><i><a href="/home" class="fa fa-tachometer fa-clr" style="color: #0f9aee;"></i></a><p class="side_icon_p">Dashborad</p></li>
  <li style="display:flex;"><i><a href="/us-cust" class="fa fa-list fa-clr" style="color: #ffc107;"></i></a><p class="side_icon_p1">Profile</p></li>
  <li style="display:flex;"><i><a href="/us-order" class="fa fa-cart-arrow-down fa-clr" style="color: #ff5722!important;"></i></a><p class="side_icon_p1">Orders</p></li>
  <li style="display:flex;"><i><a href="/us-invoice" class="fa fa-list fa-clr" style="color: #009688!important;"></i></a><p class="side_icon_p1">Invoice</p></li>
  <li style="display:flex;"><i><a href="/us-host" class="fa fa-cart-arrow-down  fa-clr "style="color:#13ffbd!important;"></i></a><p class="side_icon_p1">Hosting</p></li>
</ul>
<div>
</div><div>
</div><div>
</div>
<!-- test nav -->

</div>
<!-- enf test nav -->
<div class="common_bg">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="mysmartCloud_heading">
          MySmart Cloud
        </h1>
      </div>
    </div>
  </div>
</div>
<div class="nav-con">
  
<!-- start content -->
<div class="ad-content">
  <div class="row">
    <div class="col-sm-6">
 <div class="card card_invoice">
   <div class="card-header card-header1">
   Invoice Detail
   </div>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
    <h5><i class="fa fa-shopping-cart fa-3x card_font_icon" aria-hidden="true"></i></h5>
    </div>
    <div class="col-sm-6">
    <p class="card-text"><h2 class="invoice_h2">{{$invoices}} Invoice</h2></p>
    </div>
</div>
    <a href="/us-invoices" class="button-sm pull-right">Show Detail</a>
  </div>
 </div>
</div>
<div class="col-sm-6">
 <div class="card card_order" >
 <div class="card-header card-header1">
   Order Detail
   </div>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
    <h5><i class="fa fa-shopping-cart fa-3x card_font_icon" aria-hidden="true"></i>
</h5></div>
<div class="col-sm-6">
    <p class="card-text"><h2 class="invoice_h2">{{$orders}} orders</h2></p>
</div></div>
    <a href="/us-order" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
</div>
</div>
  <!---<div class="card">
    <h5 class="card-header table-clr">Website Overview</h5>
      <div class="card-body">
       <div class="row">


    <div class="col-sm-6" >
    <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5><i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
</h5>
    <p class="card-text"><h2>{{$invoices}} Invoice</h2></p>
    <a href="/us-invoices" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
    <div class="col-sm-5" >
    <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5><i class="fa fa-shopping-cart fa-3x" aria-hidden="true"></i>
</h5>
    <p class="card-text"><h2>{{$orders}} orders</h2></p>
    <a href="/us-order" class="button-sm pull-right">Show Detail</a>
  </div>
</div>
    </div>
       </div>
      </div>
    </div>
<!-- </div> -->
<!-- table start -->
<!-- <div class="table-mar"> -->
  <br><br><br>
<!-- <div class="ad-content"> -->

<!-- saart modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Mail Sending</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
    <form action="#" mathod="post">
      <div class="modal-body table">
        <label>Mail-Address</label><input type="text" name="email" value="mathtn@gmail.com">
        <br><br>
        Message&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea> mathan</textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" type="button" class="button-sm" value="SendMail">
      </div>
    </form>
    </div>
  </div>
</div>
</div>
<!-- end modal -->
</div>
<!-- end content -->
<div class="footer"><center>Developed by Anlyz360 
</center>
  
</div>
@endsection

