<!-- start -->
@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
<!-- test nav -->
 <style>
 body
{
  
}
input {
      border-top-style: hidden;
      border-right-style: hidden;
      border-left-style: hidden;
      border-bottom-style: groove;
      background-color: #eee;
      }
.modal-body
{
    letter-spacing: 1px;
    font-size: 18px;
}
.modal-content {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 955px;
    margin-left: -227px;
    pointer-events: auto;
    /* margin-left: auto; */
    /* margin-right: 33px; */
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: .3rem;
    outline: 0;
}

         .card1 {
         position: relative;
         display: -ms-flexbox;
         display: flex;
         -ms-flex-direction: column;
         flex-direction: column;
         min-width: 0;
         width: 100%;
         border: none;
       -webkit-box-shadow: 0 4px 9px 0 rgba(67,65,79,.1);
         box-shadow: 0 4px 9px 0 rgba(67,65,79,.1);
         border: solid 2px #f5f5f5;
         word-wrap: break-word;
         background-color: #fff;
         overflow: hidden;
         border-radius: 10px;
         background-clip: border-box;
        
         }
         .card-head1
         {
         background: #1580de;
         padding: 13px 10px;
         height:149px;
         }
         .ads,.ads1
         {
         color: white;
         line-height: 9px;
         font-size: 12px;
         letter-spacing: 2.2px;
         }
         .ads1
         {
         color: rgb(26, 25, 25);
         }
         .adrs_content2
         {
            padding-right: 43px;
         }
         .bill_procedure
         {
         /* padding: 40px 0px; */
         margin: 10px 0px 12px 32px;
         }
         .lines
         {
          height: 2px;
          width: 100%;
          background-color: #17a2b8;
         }
         .bill_amount_footer
         {
         width: 104%;
         margin-left: -20px;
         margin-top: 15px;
        
         }
         .bill_amount_footer_right
         {
         width: 100%;
         height: 125px;
         color: rgb(15, 15, 15);
         /* display: unset; */
         padding: 0px 39px;
         }
         .bill_amount_footer_left
         {
         background-color: none;
         width: 100%;
         height: 125px;
         color: white;
         /* display: unset; */
         padding: 14px 37px;
         }
         .table td {
            padding: .75rem;
            text-align: center;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
            border-bottom: 1px solid #dee2e6;

            }
            .table th
            {
               border-bottom: 1px solid #dee2e6;
            }
         .amount 
         {
         float: right;
         right: 34px;
         top: 77px;
         font-weight: 800;
         letter-spacing: 2px;
         }
         .total_amount
         {
            letter-spacing: 2px;
            font-weight: bold;
            color: #007bff;
         }
 </style>
<nav class="navbar navbar-expand-lg navbar-top1 " style="position:fixed"> 

<a class="navbar-brand" href="/">cloud Logo</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="container containner-width">  
<form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2 search-ds" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav ml-auto">
    <li class="nav-item ">
      <a class="nav-link" href="#"><i class="fa fa-envelope fa-clr" style="color: #001f8e;">&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-success">1</span> &nbsp;</i></i></a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="#">
      <i class="fa fa-bell fa-clr" style="color: #001f8e;">&nbsp;&nbsp;&nbsp;
      <span class="badge badge-pill badge-success">1</span> &nbsp;</i></a>
    </li>
    <li class="nav-item dropdown ">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-exclamation-circle fa-clr" style="color: #001f8e;"></i>&nbsp;&nbsp;&nbsp;
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </li>
    <li class="nav-item">
     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    </li>
  </ul>
  <div class="form-inline my-2 my-lg-0">
  @if(Auth::check())
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
    <i class="fa fa-user fa-clr"></i>&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
                              </a>

                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>  
    @endif   
</div>
</div>
</nav>
<!-- enf test nav -->
<div class="nav-vn">
<a class="active" href="/home"><img src="/images/logo.png" height="30px"></a>


<ul style="    list-style: none;
    padding-top: 70px;
    padding-left: 12px;
    padding-right: 10px;
    text-align: left;
    color: black;
    font-family:Poppins;
      letter-spacing: 1px;
">
  <p class="interface">Interface</p>
  <li style="display:flex;"><i><a href="/home" class="fa fa-tachometer fa-clr" style="color: #0f9aee;"></i></a><p class="side_icon_p">Dashborad</p></li>
  <li style="display:flex;"><i><a href="/us-cust" class="fa fa-list fa-clr" style="color: #ffc107;"></i></a><p class="side_icon_p1">Profile</p></li>
  <li style="display:flex;"><i><a href="/us-order" class="fa fa-cart-arrow-down fa-clr" style="color: #ff5722!important;"></i></a><p class="side_icon_p1">Orders</p></li>
  <li style="display:flex;"><i><a href="/us-invoice" class="fa fa-list fa-clr" style="color: #009688!important;"></i></a><p class="side_icon_p1">Invoice</p></li>
  <li style="display:flex;"><i><a href="/us-host" class="fa fa-cart-arrow-down  fa-clr "style="color:#13ffbd!important;"></i></a><p class="side_icon_p1">Hosting</p></li>
</ul>
<div>
</div><div>
</div><div>
</div>

<!-- test nav -->

</div>
<!-- enf test nav -->
<div class="common_bg">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="mysmartCloud_heading">
        Profile
        </h1>
      </div>
    </div>
  </div>
</div>

<div class="nav-con">
 
<!-- start content -->
<div class="ad-content ad-content1">
<!-- </div> -->
<!-- table start -->
<!-- <div class="table-mar"> -->
  <br><br><br>
<!-- <div class="ad-content"> -->
<div class="card">
<h5 class="card-header card-header01 table-clr">Invoice List</h5>
<br>
<table class="table">
  <thead class="table_head">
    <tr>
      <th scope="col">invoice ID</th>
      <th scope="col">User ID</th>
      <th scope="col">Product ID</th>
      <th scope="col">Order ID</th>
      <th scope="col">View Invoice</th>
    </tr>
  </thead>
  <tbody>
@foreach($invoices as $invoice)
    <tr>
      <td scope="col">{{ $invoice->id }}</td>
      <td scope="col">{{ $invoice->user_id }}   
       </td>
      <td scope="col">{{ $invoice->product_id }}</td>
      <td scope="col">{{ $invoice->order_id }}</td>
      <td scope="col"><button class="btn btn-primary" data-id='{{ $invoice->id }}' data-name='{{ $invoice->user->name }}' data-cname='{{ $invoice->user->cname }}' data-email='{{ $invoice->user->email }}'data-gst='{{ $invoice->user->gst }}'data-add='{{ $invoice->user->add }}'data-add1='{{ $invoice->user->add1 }}'
        data-city='{{ $invoice->user->city }}'data-zip='{{ $invoice->user->zip }}'data-state='{{ $invoice->user->state }}'data-country='{{ $invoice->user->country }}' data-in_gst='{{ $invoice->order->gst }}' data-in_total_price='{{ $invoice->order->total_price }}'
        data-date='{{$invoice->created_at}}'  data-product_name='{{ $invoice->product->product_name }}' data-package='{{ $invoice->product->package_name }}' data-description='{{ $invoice->product->description }}'  data-price='{{ $invoice->product->price }}' data-toggle="modal" data-target="#adinvoice">click</button></td>

    </tr>
    @endforeach
  </tbody>
</table>
</div>
<!-- end table -->
<!-- detail modal -->
<!-- Modal -->
<div class="modal fade" id="adinvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container">
              <div class="row">
                <div class="card card1">
                   <div class="card-head card-head1">
                      <div class="row">
                         <div class="col-sm-9">
                           <h1 style="color:white;font-size: 54px;
                           letter-spacing: 3px;">Logo</h1>
                        </div>
                         <div class="col-sm-3">
                            <h1 class="company_name" style="color: white;
                               letter-spacing: 2px;">Anlyz360</h1>
                            <div class="adrs_content">
                               <p class="ads">12,MGR nagar,</p>
                               <p class="ads">KK nagar,</p>
                               <p class="ads">Chennai-58</p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="card-body">
                      <div class="bill_procedure">
                         <div class="row">
                            <div class="col-sm-3">
                               <h3 style="color: black;font-size: 16px;">Billed To:</h3>
                               <h1 class="company_name" id="cname" style="color:black;font-size: 19px;;
                                  letter-spacing: 2px;"></h1>
                                  <p class="ads1" id="add"></p>
                                  <p class="ads1" id="add1"></p>
                                  <p><span class="ads1" id="city"></span><span class="ads1" id="zip"></span></p>
                                  <p class="ads1" id="country"></p>
                              
                            </div>
                           
                            <div class="col-sm-6">
                               <div class="adrs_content2">
                                  <h1 class="" style="color: black;
                                     letter-spacing: 2px;
                                     font-size: 15px;
                                     font-weight: 800;">Invoice Number:</h1>
                                  <p class="ads1" id="id"></p>
                                  <br>
                                  <h1 class="" style="color: black;
                                     letter-spacing: 2px;
                                     font-size: 15px;
                                     font-weight: 800;">Date Of Issue:</h1>
                                  <p class="ads1"  id="date"></p>
                               </div>
                            </div>
                            <div class="col-sm-3">
                             <!-- <h1 style=" font-size:15px;">Invoice Total</h1>
                             <h1 class="total_amount" >$5580.00</h1> -->
                           </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-sm-12">
                            <div class="lines"></div>
                         </div>
                      </div>
                      <div class="bill_details">
                         <table class="table">
                            <thead>
                               <tr style="text-align: center;">
                                  <th scope="col" style="text-align: initial;">Product id</th>
                                  <th scope="col">Product Name</th>
                                  <th scope="col">Description</th>
                                  <th scope="col">Price</th>
                                  <th scope="col">Tax value</th>
                                  <th scope="col">Total Amount</th>
                               </tr>
                            </thead>
                            <tbody>
                               <tr>
                                  <td scope="row" id="product"><br></th>
                                  <td scope="row" id="product_name"><br></th>

                                  <td id="description"></td>
                                  <td id="price"></td>
                                  <td id="gst"> <br> <br> <br</td>
                                  <td id="total_price"><br> <br> <br><br></td>

                               </tr>

                            </tbody>
                         </table>
                      </div>
                      <div class="bill_amount_footer">
                         <div class="row">
                            <div class="col-sm-6">
                               <div class="bill_amount_footer_left"></div>
                            </div>
                            <div class="col-sm-6">
                               <div class="bill_amount_footer_right">
                                  <div class="row">
                                     <div class="col-sm-6" >
                                  <span > <br> <br> <br><br> Total Price: Rs.</span><span id="tot"></span>
                                  </div>
                                  <div class="col-sm-6">
                                  <h4 class="amount"></h4>
                               </div>
                              
                              
                              </div>
                            
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!-- end detail modal -->

</div>
<!-- end content -->

@endsection


