<!-- start -->
@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
<!-- test nav -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <nav class="navbar navbar-expand-lg navbar-top1 " style="position:fixed"> 

<a class="navbar-brand" href="/">cloud Logo</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="container containner-width">  
<form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2 search-ds" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav ml-auto">
    <li class="nav-item ">
      <a class="nav-link" href="#"><i class="fa fa-envelope fa-clr" style="color: #001f8e;">&nbsp;&nbsp;&nbsp;<span class="badge badge-pill badge-success">1</span> &nbsp;</i></i></a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="#">
      <i class="fa fa-bell fa-clr" style="color: #001f8e;">&nbsp;&nbsp;&nbsp;
      <span class="badge badge-pill badge-success">1</span> &nbsp;</i></a>
    </li>
    <li class="nav-item dropdown ">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-exclamation-circle fa-clr" style="color: #001f8e;"></i>&nbsp;&nbsp;&nbsp;
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </li>
    <li class="nav-item">
     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    </li>
  </ul>
  <div class="form-inline my-2 my-lg-0">
  @if(Auth::check())
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
    <i class="fa fa-user fa-clr"></i>&nbsp;{{ Auth::user()->name }} <span class="caret"></span>
                              </a>

                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>  
    @endif   
</div>
</div>
</nav>

<!-- enf test nav -->
<div class="nav-vn">
<a class="active" href="/home"><img src="/images/logo.png" height="30px"></a>

<ul style="    list-style: none;
    padding-top: 70px;
    padding-left: 12px;
    padding-right: 10px;
    text-align: left;
    color: black;
    font-family:Poppins;
      letter-spacing: 1px;
">
  <p class="interface">Interface</p>
  <li style="display:flex;"><i><a href="/home" class="fa fa-tachometer fa-clr" style="color: #0f9aee;"></i></a><p class="side_icon_p">Dashborad</p></li>
  <li style="display:flex;"><i><a href="/us-cust" class="fa fa-list fa-clr" style="color: #ffc107;"></i></a><p class="side_icon_p1">Profile</p></li>
  <li style="display:flex;"><i><a href="/us-order" class="fa fa-cart-arrow-down fa-clr" style="color: #ff5722!important;"></i></a><p class="side_icon_p1">Orders</p></li>
  <li style="display:flex;"><i><a href="/us-invoice" class="fa fa-list fa-clr" style="color: #009688!important;"></i></a><p class="side_icon_p1">Invoice</p></li>
  <li style="display:flex;"><i><a href="/us-host" class="fa fa-cart-arrow-down  fa-clr "style="color:#13ffbd!important;"></i></a><p class="side_icon_p1">Hosting</p></li>
</ul>
<div>
</div><div>
</div><div>
</div>
<!-- test nav -->

</div>
<div class="common_bg">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="mysmartCloud_heading">
        Profile
        </h1>
      </div>
    </div>
  </div>
</div>
<div class="nav-con">
  
<!-- start content -->
<!-- <div class="ad-content"> -->
<!-- </div> -->
<!-- table start -->

<div class="ad-content">

<div class="card">
<h5 class="card-header card-header01 table-clr">Customer List</h5>
<br>
<table class="table ">
  <thead class="table_head">
    <tr>
      <th scope="col">User ID</th>
      <th scope="col">Customer Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobile Number</th>
      <th scope="col">Show Full Detail</th>
    </tr>
  </thead>
  <tbody>
@foreach($users as $user)
    <tr>
      <th >{{ $user->id }}</th>
      <td>{{ $user->name }}</td>
      <td>{{ $user->email }}</td>
      <td>{{ $user->mbl }}</td>
      <td>
      <button type="button" class="btn btn-primary" data-name="{{$user->name}}" data-cname="{{$user->cname}}" 
        data-email="{{$user->email}}" data-gst="{{$user->gst}}" data-add="{{$user->add}}"data-add1="{{$user->add1}}"
        data-city="{{$user->city}}" data-zip="{{$user->zip}}"data-state="{{$user->state}}"data-country="{{$user->country}}"
        data-mbl="{{$user->mbl}}" data-toggle="modal" data-target="#custdetail">
  click
</button>
      </td>
   


    </tr>
    @endforeach
  </tbody>
</table>
</div>
 </div> 
 <!-- Modal -->
 <div class="modal fade" id="custdetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content content-wd">
      <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Customer Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table">
          <tr>
            <td>
              <label for="name">Name</label>
            </td>
            <td>  
              <input type="text" name="name" id="name" readonly>
            </td>
          </tr> 
          <tr>
            <td>
            <label for="cname">Company Name</label>

            </td>
            <td>
            <input type="text" name="cname" id="cname" readonly>

            </td> 
          </tr>
          <tr>
            <td>
            <label for="email">Email Address</label>
            </td>
            <td>
            <input type="text" name="email" id="email"readonly>
            </td>
          </tr>
          <tr>
            <td>
            <label for="gst">GST ID</label>
            </td>
            <td>
            <input type="text" name="gst" id="gst"readonly>
            </td>
          </tr>
          <tr>
            <td>
            <label for="mbl">Mobile Number</label>
            </td>
            <td>
            <input type="text" name="mbl" id="mbl" readonly>
            </td>
          </tr>
          <tr>
            <td>
            <label for="add">Address</label>

            </td>
            <td>
            <input type="text" name="add" id="add" readonly>

            </td>
          </tr>
          <tr>
            <td>
            <label for="add1">Address</label>

            </td>
            <td>
            <input type="text" name="add1" id="add1" readonly>

            </td>
          </tr>
          <tr>
            <td>
            <label for="city">City</label>
            </td>
            <td>
            <input type="text" name="city" id="city" readonly>
            </td>
          </tr>
          <tr>
            <td>
            <label for="zip">Zip Code</label>
            </td>
            <td>
            <input type="text" name="zip" id="zip" readonly>
            </td>
          </tr>
          <tr>
            <td>
            <label for="state">State</label>
            </td>
            <td>
            <input type="text" name="state" id="state" readonly>
            </td>
          </tr>
          <tr>
            <td>
            <label for="country">Country</label>
            </td>
            <td>
            <input type="text" name="country" id="country" readonly>
            </td>
          </tr>
        <table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- end modal -->



<!-- end content -->



@endsection


