@extends('cartlayout')
 
@section('title', 'Cart')
 
@section('content')

<!-- start top nav -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="container containner-width">
  <a class="navbar-brand" href="#">MySmart Cloud logo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Currency
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown" >
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-globe"></i>
        Language
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item" style="display: block ruby;">
      <i class="fa fa-user fa-h"></i>
        <a class="nav-link fa-s" href="#">Login/Signup</a>
      </li>
      </li>
      <li class="nav-item " style="display: block ruby;">
        <i class="fa fa-shopping-cart fa-h" ></i>
         
        <a class="nav-link fa-s" href="check-out">Cart   @if(Session::has('cart'))<span class="badge badge-pill badge-success">@if(session('cart'))        
<?php                 
          $sum = 0;
          foreach(session('cart') as $id => $details)
          {
          $sum+= $details['quantity'];
          } 
          echo $sum;
?>
  @endif</span>@endif</a>
      </li>
      
    </ul>
   
  </div>
</div>
</nav>
<!-- end top nav -->
<!-- start base nav -->

<nav class="navbar navbar-expand-lg navbar-light bg-light navbar navbar-dark bg-dark">
<div class="container containner-width">  
<a class="navbar-brand" href="#">Dropdown </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link active" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Domain</a>
      <div class="dropdown-content">
        <a href="#">Registration</a>
        <a href="#">Add-ons</a>
        <a href="#">Transfer</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Website<span class="badge badge-pill badge-success">new</span></a>
      <div class="dropdown-content">
        <a href="#">Build your Website</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Hosting</a>
      <div class="dropdown-content">
        <a href="host">Shared Hosting</a>
        <a href="#">Servers</a>
        <a href="#">Reseller Hosting</a>
        <a href="#">Tools</a>
    </div>
    </div>
      </li>
      <li class="nav-item">
        <a class="nav-link dropdown" href="cloud">Cloud</a>
      </li>
      <li class="nav-link">
      <div class="email">
      <a class="emailbtn">Email</a>
      <div class="email-content">
        <a href="#">G Suite<br>(formerly Google Apps for Work)</a>
        <a href="#">Business Email</a>
        <a href="#">Enterprise Email</a>
    </div>
    </div>
      </li>
    </ul>   
  </div>
  <div>
</nav>
<!-- end base nav -->
<br>
<div class="container">
<div class="row">
        <div class="col-lg-12 col-sm-12 col-12 main-section">
            <div class="dropdown">
                <button type="button" class="btn btn-info" data-toggle="dropdown">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger"></span>
                </button>
                <div class="dropdown-menu">
                    <div class="row total-header-section">
                        <div class="col-lg-6 col-sm-6 col-6">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">3</span>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                            <p>Total: <span class="text-info">$2,978.24</span></p>
                        </div>
                    </div>
                    <div class="row cart-detail">
                        <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                            <img src="https://images-na.ssl-images-amazon.com/images/I/811OyrCd5hL._SX425_.jpg">
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                            <p>Sony DSC-RX100M..</p>
                            <span class="price text-info"> $250.22</span> <span class="count"> Quantity:01</span>
                        </div>
                    </div>
                    <div class="row cart-detail">
                        <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                            <img src="https://cdn2.gsmarena.com/vv/pics/blu/blu-vivo-48-1.jpg">
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                            <p>Vivo DSC-RX100M..</p>
                            <span class="price text-info"> $500.40</span> <span class="count"> Quantity:01</span>
                        </div>
                    </div>
                    <div class="row cart-detail">
                        <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                            <img src="https://static.toiimg.com/thumb/msid-55980052,width-640,resizemode-4/55980052.jpg">
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                            <p>Lenovo DSC-RX100M..</p>
                            <span class="price text-info"> $445.78</span> <span class="count"> Quantity:01</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                            <a href="{{ url('cart') }}" class="btn btn-primary btn-block">View all</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:20%">Product</th>
            <th style="width:20%">Package Name</th>
            <th style="width:10%">description</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
 
        <?php $total = 0 ?>
 
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
            <form action="pay" method="post">
              @csrf
                <?php $total += $details['price'] * $details['quantity'] ?>
                <tr>
                    <td data-th="Product">
                        
                            {{ $details['name'] }}
                           
                        
                    </td>
                    <td>{{ $details['description'] }}</td>
                    <td>{{ $details['description'] }}</td>

                    <td data-th="Price">{{ $details['price'] }}</td>
                    <td data-th="Quantity">
                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">{{ $details['price'] * $details['quantity'] }}</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                    </td>
             
            @endforeach
        @endif
 
        </tbody>
        <tfoot>
        <tr class="visible-xs">
            <!-- <td class="text-center"><strong>Total {{ $total }}</strong></td> -->
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="1" class="hidden-xs"><strong>Total</strong></td>
            <td class="hidden-xs text-center"><strong> {{ $total }}</strong></td>
        </tr>
        </tfoot>
    </table>
    <input type="submit" class="button">
    </form>

    
    <br><br>
    <center><buttton><a href="{{ url('/') }}" class="btn btn-warning button"><i class="fa fa-angle-left"></i> Continue Shopping</a></button></center>
 <div>
@endsection
@section('scripts')
    <script type="text/javascript">
 
        $(".update-cart").click(function (e) {
           e.preventDefault();
 
           var ele = $(this);
 
            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });
 
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
 
            var ele = $(this);
 
            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
 
    </script>
 
@endsection