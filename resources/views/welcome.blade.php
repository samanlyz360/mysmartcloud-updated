@extends('master.master')
@section('title')
    MySmart Cloud
@endsection
@section('content')
    <!-- start top nav -->
<nav class="navbar navbar-expand-lg navbar-light ">
<div class="container containner-width">
<i class="fa fa-envelope"></i>&nbsp;support@mysmartcloud.com
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Currency
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown" >
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-globe"></i>
        Language
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item" style="display: block ruby;">
        @if(Auth::check())
          @else
            <i class="fa fa-user fa-h"></i>
            <a class="nav-link fa-s" href="login">Login/Signup</a>
         @endif
      </li>
      </li>
      <li class="nav-item" style="display: block ruby;">
        <i class="fa fa-shopping-cart fa-h" ></i>
        
        <a class="nav-link fa-s" href="check-out">Cart   @if(Session::has('cart'))<span class="badge badge-pill badge-success">new</span>@endif</a>
      </li>
      
    </ul>
   
  </div>
</div>
</nav>
<!-- end top nav -->
<!-- start base nav -->

<nav class="navbar navbar-expand-lg navbar-light bg-light navbar navbar-dark bg-dark">
<div class="container containner-width">  
<a class="navbar-brand" href="#">Dropdown </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link active" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Domain</a>
      <div class="dropdown-content">
        <a href="#">Registration</a>
        <a href="#">Add-ons</a>
        <a href="#">Transfer</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Website<span class="badge badge-pill badge-success">new</span></a>
      <div class="dropdown-content">
        <a href="#">Build<span style="color:transparent">_</span>your<span style="color:transparent">_</span>Website</a>
    </div>
    </div>
      </li>
      <li class="nav-link">
      <div class="dropdown">
      <a class="dropbtn">Hosting</a>
      <div class="dropdown-content">
        <a href="host">Shared Hosting</a>
        <a href="#">Servers</a>
        <a href="#">Reseller Hosting</a>
        <a href="#">Tools</a>
    </div>
    </div>
      </li>
      <li class="nav-item">
        <a class="nav-link dropdown" href="cloud">Cloud</a>
      </li>
      <li class="nav-link">
      <div class="email">
      <a class="emailbtn">Email</a>
      <div class="email-content">
        <a href="#">G Suite<br>(formerly Google Apps for Work)</a>
        <a href="#">Business Email</a>
        <a href="#">Enterprise Email</a>
    </div>
    </div>
      </li>
    </ul>  
    @if(Auth::check())
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="margin:-3px 57px 0px 0px;">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>  
      @endif   
  </div>
  <div>
</nav>
<br>
@if(session()->has('cart-empt'))
    <div class="alert alert-success"> 
    {!! session('cart-empt') !!}
    </div>
@endif
<!-- end base nav -->
<div class="row-style">
<div class="container">
<div class="row">
<div class="col-sm-7">
    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active" data-interval="10000">
                <img src="/images/1.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item" data-interval="2000">
                 <img src="/images/1.png" class="d-block w-100" alt="...">
             </div>
            <div class="carousel-item">
                 <img src="/images/1.png" class="d-block w-100" alt="...">
            </div>
        </div>
            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                 <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
             </a>
    </div>
</div>
  <!-- <img src="/images/1.png"> -->
    <div class="col-sm-5"> 
        <div class="card-1">
            <h1>The Best Web Hosting</h1> 
           <div class="card-style">
                <i class="fa fa-check-circle fa1">&nbsp;</i>FREE Domain and Site Builder<br>
                <i class="fa fa-check-circle"></i>&nbsp;24/7 Support<br>
                <i class="fa fa-check-circle"></i>&nbsp; 1-Click WordPress Install<br>
                <i class="fa fa-check-circle"></i>&nbsp;30-Day Money-Back Guarantee<br><br>
            </div>
               <h6>Start Building your web presence at <span class="amt-font">Rs.130.68/mo</span></h6><br><br>
               <a href="/host"><button class="button pull-right"><span>Start Here</span></button></a>
        </div>
        </div>
  
</div>
<div>
</div>
 </div>
</div>

<div class="host1">
<br><br>
<h1><center>Get your business online today</center></h1>
    <center>99% uptime for rock-solid performance</center>
    <div><br><br>
    <div class="container">
        <div class="card-deck">
            <div class="card">
                <h5 class="card-title">
                    <center>Web Hosting
                    <br>
                    Email Hosting included</center>
                </h5>
        <div class="card-body">
            <img src="/images/h1.jpg" class="card-img-top" alt="...">
            <p class="card-text">
                 99.9% Uptime Guarantee<br>
                 Reliable & Secure<br>
                 Powered by cPanel / Plesk
            </p><br><br>
      <p class="card-text"><a href="/host"><button class="button pull-right"> <span>Start Here</span></button></a></p>
    </div>
  </div>
  <div class="card">
  <h5 class="card-title"><center>Reseller Hosting <br>  Free WHMCS Included</h5><center>
    <div class="card-body">
    <img src="/images/h1.jpg" class="card-img-top" alt="...">
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p><br><br><br>
      <p class="card-text"><button class="button pull-right"><span>Start Here</span></button></p>
    </div>
  </div>
  <div class="card">
  <h5 class="card-title"><center>Build your website
        <br>
        No technical skills required</h5></center>
    <div class="card-body">
    <img src="/images/h1.jpg" class="card-img-top" alt="...">
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
      <p class="card-text"><button class="button pull-right"><span>Start Here</span></button></p>
    </div>
  </div>
</div>
</div>
<br><br>
<div class="domain">
    <div class="container">
        <div class="row">
            <div class="col-sm-6"> 
                <img src="/images/2.png">
            </div>
            <div class="col-sm-6 com1"> 
                <p>Professional Email</p>
                <h2><bold>Earn your customers’ trust.</bold></h2>

                A professional email address that matches your domain shows customers that you take your business seriously. 
                Plus, it helps you stay organized with a built-in calendar, address book and task lists.
                <br><br>
                <button class="button pull-right">Learn More</button>
            </div>
        </div>
    </div>
</div>

<div class="foot">
  <div class="container">
     <div class="row">
        <div class="col-sm-3">
         <div class="service-app">Application Services</div>
         <h4 class="contact"><div class="contact1">Managed Applicatin Services</div>
          <div class="contact1">Business Analytics</div>
          <div class="contact1">Application Development</h4>
         </div>
         <div class="col-sm-3">
            <div class="service-app">Managed Cloud<br></div>
            <h3 class="contact">
              <div class="contact1">Microsoft Azure</div>
              <div class="contact1">Amazon Web Services</div>
              <div class="contact1">Google Cloud Platform</div>
              <div class="contact1">Kubemetes</div></h3>
         </div>
         <div class="col-sm-3">
            <div class="service-app">Managed Services</div>
            <h3 class="contact">
              <div class="contact1">Hosting Services</div>
              <div class="contact1">Managed Infrastructure</div>
              <div class="contact1">DevOps Solutions</div>
              <div class="contact1">Private Services</div>
              <div class="contact1">Architecture</div>
              <div class="contact1">Migration</div>
               </h3>
         </div>
        <div class="col-sm-3">
          <div class="service-app">ABOUT & Support</div>
            <h3 class="contact">
              <div class="contact1">Why Anlyz360</div>
              <div class="contact1">Careers</div>
              <div class="contact1">Content Information</div>
               </h3>
         </div> 
     </div><br>
     <div class="row">
       <div class="col-sm-4">
         <div class="row">
       <div class="col-sm-3">Home</div>
       <div class="col-sm-3">Services</div>
       <div class="col-sm-3">Products</div>
       <div class="col-sm-3">About</div>
     </div>
     </div>
     <div class="col-sm-8">

     </div>
    </div>
</div>
</div>
<div class="footer">
  <h3>Developed by anlyz360</h3>
</div>
<br><br><br>
@endsection