<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//admin page process
Route::get('admin', 'AdminController@index')->name('admin.dashborad');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin-host','AdminController@showhost');
Route::any('hostpost','AdminController@hostpost');
Route::any('hostupdate','AdminController@hostupdate');
Route::get('/cust-list','AdminController@showcust');
Route::post('/cust-detail','AdminController@custdetail');
Route::get('/order','AdminController@showorder');
Route::get('/ad-Domain','AdminController@showdomain');
Route::get('/ad-website','AdminController@showweb');
Route::get('/invoice','AdminController@showinvoice');

//hosting pages
Route::get('/host','ProductController@index');
Route::any('/add-cart','ProductController@getAddToCart');
Route::get('/add-to-cart/{id}','ProductController@getAddToCart');
Route::patch('update-cart', 'ProductController@update'); 
Route::delete('remove-from-cart', 'ProductController@remove');


// user page process
// Route::get('/check-out','ProductController@check');
Route::get('/us-order','HomeController@order');
Route::get('/us-invoice','HomeController@invoice');
Route::post('/pay','HomeController@pay');
Route::post('payumoney/response','HomeController@response');
Route::get('/check-out','HomeController@check');
Route::get('/us-cust','HomeController@showcust');
Route::get('/test','HomeController@test');




// testing 

Route::get('sample','AdminController@test');
Route::any('samplepost','AdminController@samplepost');
Route::any('sampleupdate','AdminController@sampleupdate');

//Domain
Route::get('Domain-register', function () {
    return view('domain.Domain');
});



