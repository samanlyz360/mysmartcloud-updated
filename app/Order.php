<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
       'user_id', 'product_id','gst','total_price','payment_status','domain','txnid','cpanel_user','cpanel_pwd'
    ];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function product()
    {
    	return $this->belongsTo(Product::class);
    }


}
