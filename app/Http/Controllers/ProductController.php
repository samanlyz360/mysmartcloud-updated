<?php

namespace App\Http\Controllers;
use App\Product;
use App\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Session;
class ProductController extends Controller
{
   // public function __construct()
   // {
   //     $this->middleware('auth');
   // }
   public function index()
   {
    $products = Product::where("product_name","shared hosting")->take(3)->get();
    // return "hi";
   //  $products = Product::all();
    // return $products;
    return view('hosting.shared',['products'=>$products]);
   }
   public function add(Request $request)
   {
            //   return $request->all();
      //  $tax_ammount=$request->price *(18/100);
       $gst_ammount=$request->price *(18/100);
      //  $sgst_ammount=$request->price *(9/100);
       // return $tax_price;
       $total_price=$request->price+$gst_ammount ;
      //  return $total_price;
          session()->put('cart',$request->product_id);
          session()->put('pid',$request->product_id);
          session()->put('product_name',$request->product_name);
          session()->put('package_name',$request->package_name);
          session()->put('description',$request->description);
          session()->put('price',$request->price);
          session()->put('domain_name',$request->domain_name);
          session()->put('gst_ammount',$gst_ammount);
          session()->put('total_price',$total_price);

         //  return session()->get('pid');
         //     $cart=[      'pid'=>$request->pid,
         //                  'product_name'=>$request->product_name,
         //                  'package_name'=>$request->package_name, 
         //                  'description'=>$request->description,
         //                  'price'=>$request->price, 
         //                  'gst_ammount'=>$ggst_ammount,
         //                  'sgst_ammount'=>$sgst_ammount,
         //                  'total_price'=>$total_price
         //     ];


         // session()->put('product_name',$request->product_name); 
            //  return session()->get('product_name');

            //   foreach(session('cart') as $id => $details)
            //   {
            //    return $details;
            //   }
            session()->flash('addcart','Product Added Sucessfully!!!!! Please check your cart');
         return Redirect::back();
   }

   public function getAddToCart(Request $request)
   {
      // return session()->all(); 

      // return $request->all();
      $id=$request->product_id;
      $product = Product::find($id);
      // return $product;
        if(!$product) {
 
            abort(404);
 
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "name" => $product->product_name,
                        "quantity" => 1,
                        "price" => $product->price,
                        "photo" => $product->package_name,
                        "description"=>$product->description
                    ]
            ];
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
 
            $cart[$id]['quantity']++;
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
                  "name" => $product->product_name,
                   "quantity" => 1,
                  "price" => $product->price,
                  "package_name" => $product->package_name,
                  "description"=>$product->description
        ];
 
        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
      // session()->flush();
      // $id= $request->product_id;
      // $product = Product::find($id);
      // // dd($product);
      // $oldCart=Session::has('cart') ? Session::get('cart') : null;
      // // dd($oldCart);
      // $cart = new Cart($oldCart);
      // $cart->add($product,$product->id);
      // // dd($cart);
      // $request->session()->put('cart',$cart);
      return back();

   }
   public function check()
   {
         //   Session(['a'=>'mathan','b'=>'100','c'=>'200']);
         // $a = session::get('c');
                 return session()->all(); 

      //     $cart=[      'pid'=>'10',
      //                  'product_name'=>'20',
      //                  'package_name'=>'30', 
      //                  'description'=>'40',
      //                  'price'=>'50', 
      //                  'ggst_ammount'=>'60',
      //                  'sgst_ammount'=>'70',
      //                  'total_price'=>'80'
      //     ];

      // session()->put('cart',$cart); 

         // return session()->forget($cart);

      // foreach(session::get('cart') as $carts)
      //       {
      //          return $carts;
      //       }
            //   dd($cart['product_name']);
            //  return view('home.check-out');
              
   }
   public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
 
            $cart[$request->id]["quantity"] = $request->quantity;
 
            session()->put('cart', $cart);
 
            session()->flash('success', 'Cart updated successfully');
        }
    }
    public function remove(Request $request)
    {
        if($request->id) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->id])) {
 
                unset($cart[$request->id]);
 
                session()->put('cart', $cart);
            }
 
            session()->flash('success', 'Product removed successfully');
        }
    }


    
}
