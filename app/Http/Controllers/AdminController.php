<?php

namespace App\Http\Controllers;
use App\User;
use App\Order;
use App\Invoice;
use Illuminate\Http\Request;
use App\Product;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users=User::all()->count();
        $orders=Order::all()->count();
        $invoices=Invoice::all()->count();
        return view('admin',compact('users','orders','invoices'));
    }
    public function showhost()
    {
        $products = Product::all();
        return view('admin-dash.hosting',compact('products'));
      
    }
    public function hostpost(Request $request)
    {
        // return $request->all();
        Product::create($request->all());
        return back();
   
    }
    public function hostupdate(Request $request)
    {
        // return $request->all();  
        Product::find($request->product_id)->update($request->all());
        return back();
   
    }
    public function showorder()
    {
        $orders = order::all();
        // return $users;
        return view('admin-dash.order-detail',compact('orders'));
    }
    public function showcust()
    {
        $users = user::all();
        // return $users;
        return view('admin-dash.cust-list',compact('users'));
    }
    public function custdetail(Request $request)
    {
        return response()->json(['success'=>'Data is successfully added']);
    }
    public function showdomain()
    {
        return view('admin-dash.ad-domain');
    }
    public function showweb()
    {
        return view('admin-dash.ad-web');
    }
    public function showinvoice()
    {
        $invoices = Invoice::all();
        // return $users;
        return view('admin-dash.ad-invoice',compact('invoices'));
     
    }

    public function test()
    {
        // return view('admin-dash.sample');
        $products = Product::all();
        return view('admin-dash.sample',compact('products'));
    }

}
