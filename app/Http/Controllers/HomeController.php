<?php

namespace App\Http\Controllers;
use Softon\Indipay\Facades\Indipay;  
use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Product;
use Auth;
use Redirect;
use App\Invoice;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id= Auth::user()->id;
        // $orders = Order::find($id)->count();
        // return $orders;
        $orders=Order::find($id)->count();
        $invoices=Invoice::find($id)->count();
        return view('home',compact('orders','invoices'));
        // return $users;

    }
    public function order()
    {
        // return "hihih";
        // $a = User::all();
        // return $a;                      
        // $orders = Order::where('id','3')->get();
        // $orders = Order::all();
        $id= Auth::user()->id;
         $orders = Order::where('user_id',$id)->get();
        //  $orders = order::all();
        // return $orders;
        // return view('admin-dash.order-detail',compact('orders'));
        return view('home.us-order',compact('orders'));
        // return view('home');
    }
    public function invoice()
    {
         $id= Auth::user()->id;
         $invoices = Invoice::where('user_id',$id)->get();
        //  return $orders;
         return view('home.us-invoice',compact('invoices'));
        // return view('home');
    }
    public function showcust()
    {
        $id= Auth::user()->id;
        $users = User::where('id',$id)->get();        // return $users;
        // return $users;
        return view('home.cust-list',compact('users'));
    }
    public function pay(Request $request)
    {
        return $request->all();
        // $pkg_name="mathan_Single_static";
        // echo $username;
        $user_id=Auth::user()->id;
        $user= User::find($user_id);
        $txnid =uniqid();
        $cpanel_user=$user->name.rand(0,9999);
        $cpanel_pwd = uniqid();
        // return $user;
        $order = new Order();
        $order->user_id=$user_id;
        $order->product_id=$request->product_id;
        $order->gst=$request->gst_ammount;
        $order->total_price=$request->total_price;
        $order->domain=$request->domain_name;
        $order->txnid=$txnid;
        $order->cpanel_user=$cpanel_user;
        $order->cpanel_pwd=$cpanel_pwd;
        $order->save();
        $this->sesfor();
        $order_id=$order->id;
        // echo $order_txnid=$order->txnid;
        // echo $product_info=$order->product->product_name;
        // return $product_info;
        // Order::findOrFail(1)->product->product_name;
        // return $request->all();
        // return Auth::user()->email ;
        $parameters = [          
            'txnid' => $order->txnid,
            'order_id' => $order_id,
            'firstname' => $user->name,
            'lastname'=>$user->lname,
            'email' => $user->email,
            'address1'=>$user->add,
            'address2'=>$user->add1,
            'city'=>$user->city,
            'state'=>$user->state,
            'country'=>$user->country,
            'zipcode'=>$user->zip,
            'phone' => $user->mbl,
            'productinfo' => $order->product->product_name,
            'service_provider' => '',
            'amount' =>$order->total_price,
            'curl'=>url('payumoney/response'),
        ];
        // return $parameters;
          $order = Indipay::prepare($parameters);
          return Indipay::process($order);
        //   return $request->all();
    }
    public function response(Request $request)
        {
        // For default Gateway
        $response = Indipay::response($request);
        // For Otherthan Default Gateway
        // dd($response);
        // $order_id=$response['order_id'];
        if($response['status']=="success" && $response['unmappedstatus']=='captured')
        {
            $productinfo=$response['productinfo'];
            $txnid=$response['txnid'];
            $status=$response['status'];
            $order=Order::where('txnid',$txnid)
                    ->update(['payment_status'=>$status]);
            $order=Order::where('txnid',$txnid)->first();
                    $invoice = new invoice();
                    $invoice->user_id=$order->user_id;
                    $invoice->order_id=$order->id;
                    $invoice->product_id=$order->product_id;
                    $invoice->save();
                if( $productinfo =='shared hosting')
                {
                    session()->flash('create-acc','your account create sucessfully');
                    return Redirect('/check-out');
                    $user  = "mathan";
                    $token = "KQB67A3OXDZY8UOPBA1Q3FBDYPO8CEP2";
                    $email = $order->user->email;
                    $name  =$order->user->name;
                    $domain =$order->domain;
                    $username=$order->cpanel_user;
                    $pkg_name=$order->product->package_name;
                    $pwd = $order->cpanel_pwd;
                    // return $this->cpanelacc();
                    $query = "https://linuxsvr2.mysmartcloud.in:2087/json-api/createacct?api.version=1&username=$username&domain=$domain&bwlimit=unlimited&cgi=1&contactemail=$email&cpmod=paper_lantern&dkim=1&forcedns=0&frontpage=0&hasshell=1&hasuseregns=1&ip=n&language=en&owner=mathan&max_defer_fail_percentage=unlimited&max_email_per_hour=unlimited&max_emailacct_quota=1024&maxaddon=unlimited&maxftp=unlimited&maxlst=unlimited&maxpark=unlimited&maxpop=unlimited&maxsql=unlimited&maxsub=unlimited&mxcheck=auto&owner=mathan&password=$pwd&pkgname=$pkg_name&plan=mathan_Single_static&quota=500&reseller=0&savepkg=0&spamassassin=1&spf=1&spambox=y&useregns=0";
                    // return $query;
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
                    $header[0] = "Authorization: whm $user:$token";
                    curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
                    curl_setopt($curl, CURLOPT_URL, $query);
                    $result = curl_exec($curl);
                    $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                    if ($http_status != 200) 
                        {
                            session()->flash('error','while Error accour in account creations And our Support team Contact Soon ');
                            return Redirect('/check-out');
                            // echo "please Contact Our Support Team";
                            // return "[!] Error: " . $http_status . " returned\n";
                        }
                        else 
                        {
                            session()->flash('create-acc','your account create sucessfully');
                            return Redirect('/check-out');
                            // return "please check ur dashborad account has been created";
                        }            
                    curl_close($curl);
                
                }
                else
                {   
                    // other product 
                    session()->flash('pay-success','payment sucessfull and Our Support Contact soon');
                    return Redirect('/check-out');          
                    // return "Our Support callback soon";
                }
            // echo "sucess";
        }else
        {
        // echo "fail";
        session()->flash('pay-fail','payment sucessfull and Our Support Contact soon');
        return back();
        }
        // $response = Indipay::gateway('NameOfGatewayUsedDuringRequest')->response($request);
        // dd($response);
    
    }   
    public function cpanelacc()
    {
        return "cpanel account creation";
        $user = "mathan";
        $token = "KQB67A3OXDZY8UOPBA1Q3FBDYPO8CEP2";
        $email = Auth::user()->email;
        $name=Auth::user()->name;
        $domain ="mysmart.com";
        $username=$name.rand(0,9999);
        $pkg_name="mathan_Single_static";
        // echo $username;
        $pwd = uniqid();
        $query = "https://linuxsvr2.mysmartcloud.in:2087/json-api/createacct?api.version=1&username=$username&domain=$domain&bwlimit=unlimited&cgi=1&contactemail=$email&cpmod=paper_lantern&dkim=1&forcedns=0&frontpage=0&hasshell=1&hasuseregns=1&ip=n&language=en&owner=mathan&max_defer_fail_percentage=unlimited&max_email_per_hour=unlimited&max_emailacct_quota=1024&maxaddon=unlimited&maxftp=unlimited&maxlst=unlimited&maxpark=unlimited&maxpop=unlimited&maxsql=unlimited&maxsub=unlimited&mxcheck=auto&owner=mathan&password=$pwd&pkgname=$pkg_name&plan=mathan_Single_static&quota=500&reseller=0&savepkg=0&spamassassin=1&spf=1&spambox=y&useregns=0";
                   
    //api
        //  $query = "https://linuxsvr2.mysmartcloud.in:2087/json-api/createacct?api.version=1&username=smart&domain=smart.in&bwlimit=unlimited&cgi=1&contactemail=mathankumar.ar@gmail.com&cpmod=paper_lantern&dkim=1&forcedns=0&frontpage=0&hasshell=1&hasuseregns=1&ip=n&language=en&owner=mathan&max_defer_fail_percentage=unlimited&max_email_per_hour=unlimited&max_emailacct_quota=1024&maxaddon=unlimited&maxftp=unlimited&maxlst=unlimited&maxpark=unlimited&maxpop=unlimited&maxsql=unlimited&maxsub=unlimited&mxcheck=auto&owner=mathan&password=Mathan$2468&pkgname=mathan_Single_static&plan=mathan_Single_static&quota=500&reseller=0&savepkg=0&spamassassin=1&spf=1&spambox=y&useregns=0";
               
    //   return $query;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        $header[0] = "Authorization: whm $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        // echo $result;
        // echo "<br>";
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($http_status != 200) 
        {
            echo "please Contact Out Support Team";
            return "[!] Error: " . $http_status . " returned\n";
        } else 
        {
                 return "please check ur dashborad";
        }

        curl_close($curl);

    }
    public function check()
    {
                      return view('home.check-out');

            // if(session()->has('cart'))
            // {
            //   return view('home.check-out');
            // }
            //  else
            //  {
            //     session()->flash('cart-empt','Your Cart is Empty');
            //     return back();  
            //  }
    }
    public function test()
    {
              
        $txnid='5e539a08daf91';
        $status='sucess';
       
        $order=Order::where('txnid',$txnid)
                        ->update(['payment_status'=>'sucess']);
        $order=Order::where('txnid',$txnid)->first();

                $invoice = new invoice();
                $invoice->user_id=$order->user_id;
                $invoice->order_id=$order->id;
                $invoice->product_id=$order->product_id;
                $invoice->save();
        // $order->status=$status;
        // $order->save();
        return $order->gst;
        return Order::where('txnid','5e535f3f15fd2')->get();

        // return Order::findOrFail(1)->product->product_name;txnid
        
        // $txnid =uniqid();
        $email = Auth::user()->email;
        $name=Auth::user()->name;
        $domain ="domain";
        $username=rand(0,9999).$name;
        $pkg_name="mathan_Single_static";
        // echo $username;
        $pwd = uniqid();
        $query = "https://linuxsvr2.mysmartcloud.in:2087/json-api/createacct?api.version=1&username=$username&domain=$domain&bwlimit=unlimited&cgi=1&contactemail=$email&cpmod=paper_lantern&dkim=1&forcedns=0&frontpage=0&hasshell=1&hasuseregns=1&ip=n&language=en&owner=mathan&max_defer_fail_percentage=unlimited&max_email_per_hour=unlimited&max_emailacct_quota=1024&maxaddon=unlimited&maxftp=unlimited&maxlst=unlimited&maxpark=unlimited&maxpop=unlimited&maxsql=unlimited&maxsub=unlimited&mxcheck=auto&owner=mathan&password=$pwd&pkgname=mathan_Single_static&plan=mathan_Single_static&quota=500&reseller=0&savepkg=0&spamassassin=1&spf=1&spambox=y&useregns=0";
                   
        return $query;
    }
    public function sesfor()
    {
        session()->forget(['cart', 'pid','product_name','package_name','description','price','domain_name','gst_ammount','total_price']);
        // dd(session()->all());
        // return "test1";/
    }

    
}

