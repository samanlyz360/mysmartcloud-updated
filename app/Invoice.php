<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'user_id', 'order_id', 'product_id'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
