<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'product_name',
        'package_name',
        'description',
        'price',

    ];
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
